# Start with a base Golang image
FROM golang:1.20.4-alpine

# Set the working directory inside the container
WORKDIR /app

# Copy the Go mod and sum files to the working directory
COPY go.mod go.sum ./

# Download and cache the Go modules
RUN go mod download

# Copy the source code to the working directory
COPY . .

EXPOSE 8100

# Build the Go application
RUN go build -o appchat-api

# Set the entry point of the container
ENTRYPOINT ["./appchat-api"]
