package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	AUTHSTATE_UNAUTHORIZED int = 0
	AUTHSTATE_AUTHORIZED   int = 1
	AUTHSTATE_UNCOMPLETED  int = 2
	AUTHSTATE_NEW_INSTALL  int = 3
)

type User struct {
	UserId          primitive.ObjectID   `json:"userId,omitempty" bson:"_id,omitempty"`
	FullName        string               `json:"fullName,omitempty"`
	UserName        string               `json:"userName,omitempty" validate:"required"`
	Password        string               `json:"password,omitempty" validate:"required"`
	LocalName       string               `json:"localName,omitempty"`
	Email           string               `json:"email,omitempty" unique:"true"`
	Phone           string               `json:"phone,omitempty" unique:"true"`
	Exchange        int                  `json:"exchange" default:"0"`
	Avatar          string               `json:"avatar,omitempty"`
	Relationship    interface{}          `json:"relationship,omitempty"`
	Birthday        primitive.DateTime   `json:"birthday,omitempty"`
	CreatedAt       primitive.DateTime   `json:"createdAt,omitempty"`
	UpdateAt        primitive.DateTime   `json:"updateAt,omitempty"`
	IsDeleted       int                  `json:"isDeleted" default:"0"`
	IsActive        int                  `json:"isActive" default:"0"`
	IsUpdateProfile int                  `json:"isUpdateProfile" default:"0"`
	ContactUnis     []primitive.ObjectID `json:"contactUnis,omitempty"`
	Token           string               `json:"token,omitempty" bson:"-"`
}
