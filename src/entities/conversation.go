package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Conversation struct {
	GroupId        primitive.ObjectID   `json:"groupId,omitempty" bson:"_id,omitempty"`
	GroupType      int                  `json:"groupType"`
	Theme          int                  `json:"theme"`
	EmojiGroup     int                  `json:"emojiGroup"`
	LastActiceTime primitive.DateTime   `json:"lastActiceTime,omitempty"`
	CreatedAt      primitive.DateTime   `json:"createdAt,omitempty"`
	UpdateAt       primitive.DateTime   `json:"updateAt,omitempty"`
	Avatar         string               `json:"avatar,omitempty"`
	GroupStatus    int                  `json:"groupStatus"`
	OwnerUin       primitive.ObjectID   `json:"ownerUin,omitempty"`
	CreatorUin     primitive.ObjectID   `json:"creatorUin,omitempty"`
	LastMessage    Message              `json:"lastMessage,omitempty"`
	Members        []primitive.ObjectID `json:"members,omitempty"`
	RemovedMember  []primitive.ObjectID `json:"eemovedMember,omitempty"`
	MediaFiles     interface{}          `json:"mediaFiles,omitempty"`
}
