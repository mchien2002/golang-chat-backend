package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

const (
	USER_STATUS_WAITING_ACCEPT int = 0
	USER_STATUS_ACCEPTED       int = 1
	USER_STATUS_FRIEND         int = 2
	USER_STATUS_BEST_FRIEND    int = 3
)

type Contact struct {
	ContactId    primitive.ObjectID `json:"contactId,omitempty"  bson:"_id,omitempty"`
	Status       int                `json:"status"`
	FriendSeeker primitive.ObjectID `json:"friendSeeker,omitempty"`
	FriendId     primitive.ObjectID `json:"friendId,omitempty"`
	CreatedAt    primitive.DateTime `json:"createdAt,omitempty"`
	UpdatedAt    primitive.DateTime `json:"updatedAt,omitempty"`
}
