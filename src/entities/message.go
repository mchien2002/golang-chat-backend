package entities

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Message struct {
	MessageId    primitive.ObjectID   `json:"messageId,omitempty" bson:"_id,omitempty"`
	Type         int                  `json:"type"`
	Status       int                  `json:"status"`
	GroupType    int                  `json:"groupType"`
	GroupId      primitive.ObjectID   `json:"groupId,omitempty"`
	Message      string               `json:"message,omitempty"`
	CreatedAt    primitive.DateTime   `json:"createdAt,omitempty"`
	UpdateAt     primitive.DateTime   `json:"updateAt,omitempty"`
	SenderName   string               `json:"senderName,omitempty"`
	SenderUin    primitive.ObjectID   `json:"senderUin,omitempty"`
	SenderAvatar string               `json:"senderAvatar,omitempty"`
	Nonce        string               `json:"nonce,omitempty"`
	Attachment   interface{}          `json:"attachment,omitempty"`
	SeenUins     []primitive.ObjectID `json:"seenUins,omitempty"`
	DeletedUins  []primitive.ObjectID `json:"deletedUins,omitempty"`
	LikeUins     []primitive.ObjectID `json:"likeUins,omitempty"`
}
