package helpers

type Response struct {
	Status int         `json:"status"`
	Msg    string      `json:"message"`
	Error  interface{} `json:"error"`
	Data   interface{} `json:"data"`
}

type EmptyObjec struct{}

func BuildResponse(status int, message string, data interface{}) Response {
	res := Response{
		Status: status,
		Msg:    message,
		Error:  nil,
		Data:   data,
	}
	return res
}

func BuildResponseError(status int, message string, err string, data interface{}) Response {
	res := Response{
		Status: status,
		Msg:    message,
		Error:  err,
		Data:   data,
	}
	return res
}
