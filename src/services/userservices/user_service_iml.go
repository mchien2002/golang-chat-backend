package userservices

import (
	"golang-chat-backend/src/entities"
	"golang-chat-backend/src/helpers"
	"golang-chat-backend/src/reponsitories/firebaserepositories"
	"golang-chat-backend/src/reponsitories/userrepositories"
	"golang-chat-backend/src/services/jwts"
	"mime/multipart"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type userRepository struct {
	UserRepository   userrepositories.UserRepository
	JwtService       jwts.JWTService
	BucketRepository firebaserepositories.FirebaseRepository
}

// SearchUserWithRelationship implements UserService
func (r *userRepository) SearchUserWithRelationship(myId string, friendId string) (*entities.User, error) {
	return r.UserRepository.FindUserByIdWithRelationship(myId, friendId)
}

// UploadAvatar implements UserService
func (r *userRepository) UploadAvatar(userId string, avatar multipart.File) (*entities.User, error) {
	user, errFindUser := r.UserRepository.FindUserById(userId)
	if user.Avatar != "" {
		r.BucketRepository.UploadFileMultipart(user.Avatar, avatar)
	} else {
		url := uuid.New()
		r.BucketRepository.UploadFileMultipart(url.String(), avatar)
		user.Avatar = url.String() + ".jpg"
		r.UserRepository.UpdateUser(*user)
	}
	return user, errFindUser
}

// SignInForUser implements UserService
func (r *userRepository) SignInForUser(userName string, passWord string) (*entities.User, error) {
	userLogin, err := r.UserRepository.FindUserByUserName(userName)
	userLogin.Token = r.JwtService.GenerateToken(userLogin.UserId.Hex())
	if err != nil {
		helpers.Error(err.Error())
		return nil, err
	} else if validHasedPassword(userLogin.Password, passWord) {
		return userLogin, nil
	} else {
		return nil, nil
	}
}

// SignUpForUser implements UserService
func (r *userRepository) SignUpForUser(newUser entities.User) (*entities.User, error) {
	newUserInserted := &entities.User{}
	newUser.Password = hashPassword(newUser.Password)
	newUserInserted, err := r.UserRepository.SigupForUser(newUser)
	if err != nil {
		return nil, err
	}
	newUserInserted.Token = r.JwtService.GenerateToken(newUser.UserId.Hex())
	return newUserInserted, nil
}

func UserServiceIml(repository userrepositories.UserRepository, jwtServce jwts.JWTService, bucket firebaserepositories.FirebaseRepository) UserService {
	return &userRepository{
		BucketRepository: bucket,
		UserRepository:   repository,
		JwtService:       jwtServce,
	}
}

func hashPassword(password string) string {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		helpers.Error(err.Error())
	}
	return string(hashedPassword)
}

func validHasedPassword(hashedPassword string, password string) bool {
	// Validating the password
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		return false
	} else {
		return true
	}
}
