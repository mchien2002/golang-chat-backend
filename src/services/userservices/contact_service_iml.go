package userservices

import (
	"golang-chat-backend/src/entities"
	"golang-chat-backend/src/helpers"
	"golang-chat-backend/src/reponsitories/userrepositories"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type contactRepository struct {
	ContactRepository userrepositories.ContactRepository
}

// UserConfirmContactInvitation implements ContactService
func (*contactRepository) UserConfirmContactInvitation(myId string, friendId string) error {
	panic("unimplemented")
}

// UserSendContactInvitation implements ContactService
func (r *contactRepository) UserSendContactInvitation(myId string, friendId string) error {
	friendSeeker, errSee := primitive.ObjectIDFromHex(myId)
	if errSee != nil {
		helpers.Error(errSee)
		return errSee
	}
	friendIdOj, errFre := primitive.ObjectIDFromHex(friendId)
	if errFre != nil {
		helpers.Error(errFre)
		return errFre
	}
	newContact := entities.Contact{
		Status:       entities.USER_STATUS_WAITING_ACCEPT,
		FriendSeeker: friendSeeker,
		FriendId:     friendIdOj,
	}
	err := r.ContactRepository.SendContactInvitation(newContact)
	if err != nil {
		helpers.Error(err)
		return err
	} else {
		return nil
	}
}

func ContactServiceIml(repo userrepositories.ContactRepository) ContactService {
	return &contactRepository{
		ContactRepository: repo,
	}
}
