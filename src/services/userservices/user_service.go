package userservices

import (
	"golang-chat-backend/src/entities"
	"mime/multipart"
)

type UserService interface {
	SignUpForUser(newUser entities.User) (*entities.User, error)
	SignInForUser(userName string, passWord string) (*entities.User, error)
	UploadAvatar(userId string, avatar multipart.File) (*entities.User, error)
	SearchUserWithRelationship(myId string, friendId string) (*entities.User, error)
}
