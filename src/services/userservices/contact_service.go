package userservices

type ContactService interface {
	UserSendContactInvitation(myId string, friendId string) error
	UserConfirmContactInvitation(myId string, friendId string) error
}
