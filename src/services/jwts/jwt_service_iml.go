package jwts

import (
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func JWTServiceIml() JWTService {
	return &jwtService{
		issuer:    os.Getenv("ISSUER"),
		secretKey: os.Getenv("SECRET_KEY"),
	}
}

type jwtCustomClaim struct {
	UserID string `json:"userid"`
	jwt.StandardClaims
}

type jwtService struct {
	secretKey string
	issuer    string
}

// GetUserIdAuthorization implements JWTService
func (*jwtService) GetUserIdAuthorization(token string, secretKey string) (string, error) {
	claims := &jwtCustomClaim{}
	parseToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		// Ở đây, bạn cần trả về key bí mật (secret key) mà bạn đã sử dụng để tạo token ban đầu
		return []byte(secretKey), nil
	})
	if err != nil {
		return "", err
	}
	if parseToken.Valid {
		return claims.UserID, nil
	} else {
		return "", fmt.Errorf("không thể xác thực token")
	}
}

func (jwts *jwtService) GenerateToken(userID string) string {
	claims := &jwtCustomClaim{
		userID,
		jwt.StandardClaims{
			ExpiresAt: time.Now().AddDate(0, 0, 1).Unix(),
			Issuer:    jwts.issuer,
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	tk, err := token.SignedString([]byte(jwts.secretKey))
	if err != nil {
		panic(err.Error())
	}
	return tk
}

// ValidateToken implements JWTService
// Xác thực token
func (jwts *jwtService) ValidateToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method %v", t.Header["alg"])
		}
		return []byte(jwts.secretKey), nil
	})
}
