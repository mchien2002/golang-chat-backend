package jwts

import (
	"github.com/dgrijalva/jwt-go"
)

type JWTService interface {
	GenerateToken(userID string) string
	ValidateToken(token string) (*jwt.Token, error)
	GetUserIdAuthorization(token string, secretKey string) (string, error)
}
