package firebase

type FirebaseBucketService interface {
	UploadFile() error
	RetrieveFile() error
}
