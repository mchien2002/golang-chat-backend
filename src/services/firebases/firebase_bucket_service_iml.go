package firebase

import "cloud.google.com/go/storage"

type firebaseBucket struct {
	Bucket *storage.BucketHandle
}

// RetrieveFile implements FirebaseBucketService
func (*firebaseBucket) RetrieveFile() error {
	panic("unimplemented")
}

// UploadFile implements FirebaseBucketService
func (*firebaseBucket) UploadFile() error {
	panic("unimplemented")
}

func FirebaseBucketServiceIml(myBucket *storage.BucketHandle) FirebaseBucketService {
	return &firebaseBucket{
		Bucket: myBucket,
	}
}
