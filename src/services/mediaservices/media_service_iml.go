package mediaservices

import (
	"context"
	"golang-chat-backend/src/constants"
	"io/ioutil"

	"cloud.google.com/go/storage"
)

type mediaRepository struct {
	BucketFirebase *storage.BucketHandle
}

// DisplayAudioMedia implements MediaService
func (*mediaRepository) DisplayAudioMedia(url string) error {
	panic("unimplemented")
}

// DisplayImageMedia implements MediaService
func (r *mediaRepository) DisplayImageMedia(url string) ([]byte, error) {
	// Lấy tham chiếu đến ảnh từ Firebase Storage
	object := r.BucketFirebase.Object(constants.AVATAR_STORAGE + url)
	reader, errRe := object.NewReader(context.Background())
	if errRe != nil {
		return nil, errRe
	}
	defer reader.Close()
	data, errData := ioutil.ReadAll(reader)
	if errData != nil {
		return nil, errData
	}
	return data, nil
}

// DisplayVideoMedia implements MediaService
func (*mediaRepository) DisplayVideoMedia(url string) error {
	panic("unimplemented")
}

func MediaServiceIml(bucket *storage.BucketHandle) MediaService {
	return &mediaRepository{
		BucketFirebase: bucket,
	}
}
