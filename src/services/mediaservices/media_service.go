package mediaservices

type MediaService interface {
	DisplayImageMedia(url string) ([]byte, error)
	DisplayVideoMedia(url string) error
	DisplayAudioMedia(url string) error
}
