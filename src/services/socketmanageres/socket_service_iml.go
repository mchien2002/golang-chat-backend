package socketmanageres

import "github.com/gorilla/websocket"

type socektRepository struct {
	WebSocketConn *websocket.Conn
	clients       *map[*websocket.Conn]bool
}

// CreateMessage implements SocketService
func (*socektRepository) CreateMessage() {
	panic("unimplemented")
}

func SocketServiceIml(conn *websocket.Conn, clients *map[*websocket.Conn]bool) SocketService {
	return &socektRepository{
		WebSocketConn: conn,
		clients:       clients,
	}
}
