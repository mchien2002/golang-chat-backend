package userrepositories

import (
	"context"
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/entities"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type contactRepository struct {
	DbContext context.Context
	Database  *mongo.Database
}

// ConfirmContactInvitation implements ContactRepository
func (*contactRepository) ConfirmContactInvitation(contact entities.Contact) error {
	panic("unimplemented")
}

// SendContactInvitation implements ContactRepository
func (mg *contactRepository) SendContactInvitation(newContact entities.Contact) error {
	newContact.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
_, errIns := mg.Database.Collection(constants.CONTACTS_COLLECTION).InsertOne(context.Background(), newContact)
	return errIns
}

func ContactRepositoryIml(db *mongo.Client) ContactRepository {
	return &contactRepository{
		DbContext: context.TODO(),
		Database:  db.Database(os.Getenv("DB_DATABASE")),
	}
}
