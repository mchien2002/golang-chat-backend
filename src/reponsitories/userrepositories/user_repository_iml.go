package userrepositories

import (
	"context"
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/entities"
	"golang-chat-backend/src/helpers"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type userRepository struct {
	DbContext context.Context
	Database  *mongo.Database
}

// ConfirmContactInvitation implements ContactRepository
func (*userRepository) ConfirmContactInvitation(contact entities.Contact) error {
	panic("unimplemented")
}

// SendContactInvitation implements ContactRepository
func (*userRepository) SendContactInvitation(newContact entities.Contact) error {
	panic("unimplemented")
}

// FindUserByIdWithRelationship implements UserRepository
func (mg *userRepository) FindUserByIdWithRelationship(myId string, userId string) (*entities.User, error) {
	user, errUser := findUserByIdHandle(mg.Database, userId, nil)
	if errUser != nil {
		return nil, errUser
	}
	user.Password = ""
	filterContact := bson.D{
		{Key: "$or", Value: bson.A{
			bson.D{
				{Key: "friendseeker", Value: myId},
				{Key: "friendid", Value: userId},
			},
			bson.D{
				{Key: "friendseeker", Value: userId},
				{Key: "friendid", Value: myId},
			},
		}},
	}
	var contact entities.Contact
	errContact := mg.Database.Collection(constants.CONTACTS_COLLECTION).FindOne(context.Background(), filterContact).Decode(&contact)
	if errContact != nil {
		return user, nil
	}
	user.Relationship = contact
	return user, nil
}

// FindUserByUserName implements UserRepository
func (mg *userRepository) FindUserByUserName(userName string) (*entities.User, error) {
	var user entities.User

	filter := bson.M{"username": userName}

	err := mg.Database.Collection(constants.USERS_COLLECTION).FindOne(context.Background(), filter).Decode(&user)
	return &user, err
}

// UpdateUser implements UserRepository
func (mg *userRepository) UpdateUser(user entities.User) (*entities.User, error) {
	user.UpdateAt = primitive.NewDateTimeFromTime(time.Now())
	filter := bson.M{"_id": user.UserId}
	update := bson.M{"$set": user}
	_, err := mg.Database.Collection(constants.USERS_COLLECTION).UpdateOne(context.Background(), filter, update)
	return &user, err
}

// FindUserById implements UserRepository
func (mg *userRepository) FindUserById(userId string) (*entities.User, error) {
	return findUserByIdHandle(mg.Database, userId, nil)
}

// SigupForUser implements UserRepository
func (mg *userRepository) SigupForUser(newUser entities.User) (*entities.User, error) {
	newUser.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	insertResult, err := mg.Database.Collection(constants.USERS_COLLECTION).InsertOne(context.Background(), newUser)
	if err != nil {
		helpers.Error(err.Error())
		return nil, err
	}
	newUser.UserId = insertResult.InsertedID.(primitive.ObjectID)
	return &newUser, nil
}

// SigupForUser implements UserRepository

func UserRepositoryIml(dbController *mongo.Client) UserRepository {
	ctx := context.TODO()

	return &userRepository{
		DbContext: ctx,
		Database:  dbController.Database(os.Getenv("DB_DATABASE")),
	}
}

func findUserByIdHandle(db *mongo.Database, userId string, projection bson.M) (*entities.User, error) {
	var user entities.User
	findOptions := options.FindOne().SetProjection(projection)
	userIDHex, _ := primitive.ObjectIDFromHex(userId)
	filter := bson.M{"_id": userIDHex}
	err := db.Collection(constants.USERS_COLLECTION).FindOne(context.Background(), filter, findOptions).Decode(&user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			helpers.Error("Model not found: ", err)
		} else {
			helpers.Error(err)
		}
		return nil, err
	}
	return &user, nil
}
