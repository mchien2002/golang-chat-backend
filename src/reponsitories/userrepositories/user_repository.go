package userrepositories

import "golang-chat-backend/src/entities"

type UserRepository interface {
	UpdateUser(user entities.User) (*entities.User, error)
	FindUserById(userId string) (*entities.User, error)
	FindUserByUserName(userName string) (*entities.User, error)
	SigupForUser(newUser entities.User) (*entities.User, error)
	FindUserByIdWithRelationship(myId string, userId string) (*entities.User, error)
}
