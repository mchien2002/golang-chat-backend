package userrepositories

import (
	"golang-chat-backend/src/entities"
)

type ContactRepository interface {
	SendContactInvitation(newContact entities.Contact) error
	ConfirmContactInvitation(contact entities.Contact) error
}
