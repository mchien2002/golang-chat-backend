package firebaserepositories

import "mime/multipart"

type FirebaseRepository interface {
	UploadFileMultipart(fileName string, file multipart.File) error
}
