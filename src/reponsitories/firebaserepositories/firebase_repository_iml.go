package firebaserepositories

import (
	"context"
	"golang-chat-backend/src/constants"
	"io"
	"mime/multipart"

	"cloud.google.com/go/storage"
)

type firebaseConnection struct {
	BucketFirebase *storage.BucketHandle
}

// UploadFileMultipart implements FirebaseRepository
func (fb *firebaseConnection) UploadFileMultipart(fileName string, file multipart.File) error {
	obj := fb.BucketFirebase.Object(constants.AVATAR_STORAGE + fileName + ".jpg")
	w := obj.NewWriter(context.Background())
	defer w.Close()
	// Copy the file contents to the writer
	_, errUpload := io.Copy(w, file)
	return errUpload
}

func FirebaseRepositoryIml(bucket *storage.BucketHandle) FirebaseRepository {
	return &firebaseConnection{
		BucketFirebase: bucket,
	}
}
