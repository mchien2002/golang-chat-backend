package utils

import "go.mongodb.org/mongo-driver/mongo"

func IsDuplicateKeyError(err error) bool {
	if mongoErr, ok := err.(mongo.WriteException); ok {
		for _, writeErr := range mongoErr.WriteErrors {
			if writeErr.Code == 11000 { // Mã lỗi 11000 chỉ định lỗi duy nhất
				return true
			}
		}
	}
	return false
}
