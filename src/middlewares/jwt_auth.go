package middlewares

import (
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/helpers"
	"golang-chat-backend/src/services/jwts"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func AuthorJWT(jwts jwts.JWTService) gin.HandlerFunc {
	return func(cx *gin.Context) {
		authHeader := strings.TrimPrefix(cx.GetHeader("token"), "Bearer ")
		if authHeader == "" {
			response := helpers.BuildResponseError(constants.API_STATUS_TOKENNOTFOUND_ERRORTOKEN, "Yêu cầu thất bại", "Không tìm thấy token", nil)
			cx.AbortWithStatusJSON(http.StatusUnauthorized, response)
		}
		token, err := jwts.ValidateToken(authHeader)
		if token.Valid {
			return

		} else {
			response := helpers.BuildResponseError(constants.API_STATUS_ERRORTOKEN, "Token không hợp lệ", err.Error(), nil)
			cx.AbortWithStatusJSON(http.StatusUnauthorized, response)
		}
	}
}
