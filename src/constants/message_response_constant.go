package constants

const (
	MESSAGE_RESPONSE_SUCCESSFUL        = "Thành công"
	MESSAGE_RESPONSE_FAILURE           = "Thất bại"
	MESSAGE_RESPONSE_TOKEN_ILLEGAL     = "Token không hợp lệ"
	MESSAGE_RESPONSE_FIND_USER_FAILURE = "Tìm người dùng thất bại"
	MESSAGE_RESPONSE_ERROR_DATA        = "Dữ liệu mà bạn gửi có vẻ không đúng :(("
	MESSAGE_RESPONSE_ERROR_OPEN_FILE   = "Không thể mở file"
	MESSAGE_RESPONSE_UPDATE_AVATAR     = "Không thể cập nhật ảnh đại diện"
	MESSAGE_RESPONSE_USER_NOTEXIST     = "Người dùng không tồn tại"
	MESSAGE_RESPONSE_USER_EXIST        = "Người dùng đã tồn tại"
	MESSAGE_RESPONSE_PASSWORD_WRONG    = "Mật khẩu không đúng"
)
