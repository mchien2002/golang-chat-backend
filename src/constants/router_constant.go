package constants

const (
	// ENDPOINT
	USER_ENDPOINT    = "/user"
	CONTACT_ENDPOINT = "/contact"
	MEDIA_ENDPOINT   = "/media"
	IMAGE_ENDPOINT   = "/image"
	VIDEO_ENDPOINT   = "/video"
	AUDIO_ENDPOINT   = "/audio"
	// API
	USER_SIGNUP                     = "/signup"
	USER_SIGNIN                     = "/signin"
	USER_SEND_CONTACT_INVITATION    = "/send-invitation"
	USER_CONFIRM_CONTACT_INVITATION = "/confirm-invitation"
	USER_UPLOAD_AVATAR              = "/upload-avatar"
	MEDIA_DISPLAY                   = "/display"

	// MEDIA
	IMAGE_URL = "/image"
	AUDIO_URL = "/audio"
	VIDEO_URL = "/video"
)
