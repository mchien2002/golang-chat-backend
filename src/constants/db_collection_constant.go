package constants

const (
	USERS_COLLECTION         = "users"
	CONVERSATIONS_COLLECTION = "conversations"
	MESSAGES_COLLECTION      = "messages"
	CONTACTS_COLLECTION      = "contacts"
)
