package constants

var (
	// GROUP
	SOCKET_REQUEST_CREATE_GROUP  = "create_group"
	SOCKET_REQUEST_UPDATE_GROUP  = "update_group"
	SOCKET_REQUEST_MUTE_GROUP    = "mute_group"
	SOCKET_REQUEST_UN_MUTE_GROUP = "unmute_group"
	SOCKET_REQUEST_PIN_GROUP     = "pin_group"
	SOCKET_REQUEST_UN_PIN_GROUP  = "unpin_group"
	SOCKET_REQUEST_LEAVE_GROUP   = "leave_group"
	SOCKET_REQUEST_DELETE_GROUP  = "delete_group"
	SOCKET_REQUEST_LIST_GROUP    = "list_group"
	SOCKET_REQUEST_CHECK_GROUP   = "check_group"

	// MESSAGE
	SOCKET_REQUEST_DELETE_CONVERSATION      = "delete_conversation"
	SOCKET_REQUEST_CREATE_MESSAGE           = "create_message"
	SOCKET_REQUEST_UPDATE_MESSAGE           = "update_message"
	SOCKET_REQUEST_LIST_MESSAGE             = "list_message"
	SOCKET_REQUEST_DELETE_MESSAGE           = "delete_message"
	SOCKET_REQUEST_REACTION_MESSAGE         = "reaction_message"
	SOCKET_REQUEST_LIST_REACTION_MESSAGE    = "list_reaction_message"
	SOCKET_REQUEST_SEEN_LASTEST_MESSAGE     = "seen_message"
	SOCKET_REQUEST_CREATED_GROUP_SCCUSSEFUL = "created_group_successful"
	SOCKET_REQUEST_LIKE_MESSAGE             = "like_message"
)
