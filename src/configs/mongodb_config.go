package configs

import (
	"context"
	"fmt"
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/helpers"
	"os"
	"time"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func SetUpMongodbConnection() *mongo.Client {

	errEnv := godotenv.Load()
	if errEnv != nil {
		panic("Faild to load env file")
	}
	connectionString := fmt.Sprintf("mongodb+srv://%s:%s@cluster0.u2pehl6.mongodb.net/",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"))

	// Thiết lập các tùy chọn kết nối
	clientOptions := options.Client().ApplyURI(connectionString)

	// Thiết lập thời gian chờ kết nối
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Kết nối tới MongoDB
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		helpers.Error(err.Error())
	}

	// Kiểm tra kết nối thành công
	err = client.Ping(ctx, nil)
	if err != nil {
		helpers.Error(err.Error())
	} else {
		helpers.Info("CONNECTED TO MONGODB, HAVE A NICE DAY :v")
	}
	setUpUniqueIndex(client)
	helpers.Info("UNIQUE INDEXS CREATED SUCCESSFULLY")
	return client
}

func CloseConnection(client *mongo.Client) {
	// Đóng kết nối khi đã xong
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var err = client.Disconnect(ctx)
	if err != nil {
		helpers.Error(err.Error())
	} else {
		helpers.Info("CLOSED CONNECTION MONGODB, SEE YOU LATE <3")
	}
}

func setUpUniqueIndex(dbController *mongo.Client) {
	errUsers := createUniqueIndex(dbController, os.Getenv("DB_DATABASE"), constants.USERS_COLLECTION, []bson.E{
		{Key: "email", Value: 1},
		{Key: "username", Value: 1},
		{Key: "phone", Value: 1},
	})
	if errUsers != nil {
		helpers.Error("FALIED TO CREATE UNIQUE INDEX: " + errUsers.Error())
		return
	}

	errContact := createUniqueIndex(dbController, os.Getenv("DB_DATABASE"), constants.CONTACTS_COLLECTION, bson.D{
		{Key: "friendseeker", Value: 1},
		{Key: "friendid", Value: 1},
	})
	if errContact != nil {
		helpers.Error("FALIED TO CREATE UNIQUE INDEX: " + errContact.Error())
		return
	}
}

func createUniqueIndex(client *mongo.Client, databaseName, collectionName string, fields []bson.E) error {
	collection := client.Database(databaseName).Collection(collectionName)

	uniqueIndex := mongo.IndexModel{
		Keys:    getIndexKeys(fields),
		Options: options.Index().SetUnique(true),
	}

	_, err := collection.Indexes().CreateOne(context.Background(), uniqueIndex)
	if err != nil {
		return err
	}

	return nil
}

func getIndexKeys(fields []bson.E) bson.D {
	keys := bson.D{}
	for _, field := range fields {
		keys = append(keys, field)
	}
	return keys
}
