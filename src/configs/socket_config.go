package configs

import (
	"encoding/json"
	"golang-chat-backend/src/helpers"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var Upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var clients = make(map[string]*websocket.Conn) // Lưu trữ các kết nối của client

func HandleWebSocket(conn *websocket.Conn, c *gin.Context) {
	defer conn.Close()

	// var socketService socketmanageres.SocketService = socketmanageres.SocketServiceIml(conn, &clients)

	handleConnection(conn, c)
	conn.SetCloseHandler(func(code int, text string) error {
		return handleDisconnection(conn, code, text)
	})
	for {
		_, message, errMessage := conn.ReadMessage()

		if errMessage != nil {
			helpers.Error("Không thể đọc được tin nhắn")
		} else if message != nil {
			var socketRequest helpers.SocketRequest
			err := json.Unmarshal(message, &socketRequest)
			if err != nil {
				helpers.Error("Không thể convert thành SocketRequest")
			}
		}

	}
}

func handleConnection(conn *websocket.Conn, c *gin.Context) {
	id := c.GetHeader("id")
	clients[id] = conn
	helpers.Info("ADDED TO CLIENTS: ", conn.RemoteAddr(), "WITH ID USER: ", id)
}

func handleDisconnection(conn *websocket.Conn, code int, text string) error {
	go deleteClientByConn(conn)
	return nil
}

func deleteClientByConn(conn *websocket.Conn) {
	for key, value := range clients {
		if value == conn {
			delete(clients, key)
			helpers.Info("DISCONNECT WITH: ", conn)
			break
		}
	}
}
