package configs

import (
	"context"
	"golang-chat-backend/src/helpers"
	"os"
	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

func SetupFirebaseBucketStorage() *storage.BucketHandle {
	opt := option.WithCredentialsFile("firebase/serviceAccountKey.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		helpers.Error("ERROR INITIALIZING APP: %v\n", err)
		return nil
	}

	client, errClient := app.Storage(context.TODO())
	if errClient != nil {
		helpers.Error("ERROR GETTING STORAGE CLIENT: %v\n", errClient)
		return nil
	}
	bucketHandler, errBucket := client.Bucket(os.Getenv("BUCKET_NAME"))
	if errBucket != nil {
		helpers.Error("ERROR GETTING STORAGE BUCKET: %v\n", errBucket)
		return nil
	}

	helpers.Info("CONNECTED TO FIREBASE BUCKET, HAVE A NICE DAY :v")

	return bucketHandler

}
