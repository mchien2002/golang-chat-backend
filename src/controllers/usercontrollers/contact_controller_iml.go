package usercontrollers

import (
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/helpers"
	"golang-chat-backend/src/services/jwts"
	"golang-chat-backend/src/services/userservices"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
)

type contactService struct {
	ContactService userservices.ContactService
	JwtService     jwts.JWTService
}

// UserConfirmContactInvitation implements ContactController
func (*contactService) UserConfirmContactInvitation(ctx *gin.Context) {
	panic("unimplemented")
}

// UserSendContactInvitation implements ContactController
func (s *contactService) UserSendContactInvitation(ctx *gin.Context) {
	friendUin := ctx.Param("friendUin")
	authHeader := strings.TrimPrefix(ctx.GetHeader("token"), "Bearer ")
	senderUin, errFriendUin := s.JwtService.GetUserIdAuthorization(authHeader, os.Getenv("SECRET_KEY"))
	if errFriendUin != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, helpers.BuildResponseError(constants.API_STATUS_ERRORTOKEN, constants.MESSAGE_RESPONSE_TOKEN_ILLEGAL, errFriendUin.Error(), nil))
		return
	}
	err := s.ContactService.UserSendContactInvitation(senderUin, friendUin)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, helpers.BuildResponseError(constants.API_STATUS_ERRORACTION, constants.MESSAGE_RESPONSE_FAILURE, "", helpers.EmptyObjec{}))
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusOK, helpers.BuildResponse(constants.API_STATUS_SUCCESS, constants.MESSAGE_RESPONSE_SUCCESSFUL, helpers.EmptyObjec{}))
	}
}

func ContactControllerIml(ser userservices.ContactService, jwt jwts.JWTService) ContactController {
	return &contactService{
		ContactService: ser,
		JwtService:     jwt,
	}
}
