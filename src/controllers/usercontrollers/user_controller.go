package usercontrollers

import "github.com/gin-gonic/gin"

type UserController interface {
	SignUpForUser(context *gin.Context)
	SignInForUser(context *gin.Context)
	UploadUserAvatar(context *gin.Context)
	SearchUserByIdWithRelationship(ctx *gin.Context)
}
