package usercontrollers

import "github.com/gin-gonic/gin"

type ContactController interface {
	UserSendContactInvitation(ctx *gin.Context)
	UserConfirmContactInvitation(ctx *gin.Context)
}
