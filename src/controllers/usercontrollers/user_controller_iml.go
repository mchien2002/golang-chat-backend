package usercontrollers

import (
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/entities"
	"golang-chat-backend/src/helpers"
	"golang-chat-backend/src/services/jwts"
	"golang-chat-backend/src/services/userservices"
	"golang-chat-backend/src/utils"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
)

type userService struct {
	UserService userservices.UserService
	JwtService  jwts.JWTService
}

// SearchUserByIdWithRelationship implements UserController
func (s *userService) SearchUserByIdWithRelationship(ctx *gin.Context) {
	userId := ctx.Param("userId")
	authHeader := strings.TrimPrefix(ctx.GetHeader("token"), "Bearer ")
	senderUin, errFriendUin := s.JwtService.GetUserIdAuthorization(authHeader, os.Getenv("SECRET_KEY"))
	if errFriendUin != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, helpers.BuildResponseError(constants.API_STATUS_ERRORTOKEN, constants.MESSAGE_RESPONSE_TOKEN_ILLEGAL, errFriendUin.Error(), nil))
		return
	}
	userInfo, errInfo := s.UserService.SearchUserWithRelationship(senderUin, userId)
	if errInfo != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, helpers.BuildResponseError(constants.API_STATUS_ERRORACTION, constants.MESSAGE_RESPONSE_FIND_USER_FAILURE, "", helpers.EmptyObjec{}))
	} else {
		ctx.AbortWithStatusJSON(http.StatusOK, helpers.BuildResponse(constants.API_STATUS_SUCCESS, constants.MESSAGE_RESPONSE_SUCCESSFUL, userInfo))
	}

}

// UploadUserAvatar implements UserController
func (s *userService) UploadUserAvatar(ctx *gin.Context) {
	file, err := ctx.FormFile("avatar")
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helpers.BuildResponseError(constants.API_STATUS_CANNOTREAD, constants.MESSAGE_RESPONSE_ERROR_DATA, "", helpers.EmptyObjec{}))
		return
	}
	var requestBody map[string]interface{}
	if errQuery := ctx.ShouldBindJSON(&requestBody); errQuery != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helpers.BuildResponseError(constants.API_STATUS_CANNOTREAD, constants.MESSAGE_RESPONSE_ERROR_DATA, "", helpers.EmptyObjec{}))
		return
	}
	userId := requestBody["userId"].(string)

	avatarUser, err := file.Open()
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, helpers.BuildResponseError(constants.API_STATUS_CANNOTREAD, constants.MESSAGE_RESPONSE_ERROR_OPEN_FILE, "", helpers.EmptyObjec{}))
		return
	}
	defer avatarUser.Close()
	userUpdated, errUpload := s.UserService.UploadAvatar(userId, avatarUser)
	if errUpload != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, helpers.BuildResponseError(constants.API_STATUS_ERRORACTION, constants.MESSAGE_RESPONSE_UPDATE_AVATAR, "", helpers.EmptyObjec{}))
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusOK, helpers.BuildResponse(constants.API_STATUS_SUCCESS, constants.MESSAGE_RESPONSE_SUCCESSFUL, userUpdated))
	}

}

// SignInForUser implements UserController
func (s *userService) SignInForUser(ctx *gin.Context) {
	var requestBody map[string]interface{}
	if errQuery := ctx.ShouldBindJSON(&requestBody); errQuery != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, helpers.BuildResponseError(constants.API_STATUS_CANNOTREAD, constants.MESSAGE_RESPONSE_ERROR_DATA, "", helpers.EmptyObjec{}))
		return
	}
	userName := requestBody["userName"].(string)
	password := requestBody["password"].(string)

	userLogin, err := s.UserService.SignInForUser(userName, password)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusNotFound, helpers.BuildResponseError(constants.API_STATUS_NOTEXISTDATA, constants.MESSAGE_RESPONSE_USER_NOTEXIST, "", helpers.EmptyObjec{}))
		return
	} else if userLogin == nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, helpers.BuildResponseError(constants.API_STATUS_INCORRECT_DATAINPUT, constants.MESSAGE_RESPONSE_PASSWORD_WRONG, "", helpers.EmptyObjec{}))
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusOK, helpers.BuildResponse(constants.API_STATUS_SUCCESS, constants.MESSAGE_RESPONSE_SUCCESSFUL, userLogin))
	}
}

// SignUpForUser implements UserController
func (s *userService) SignUpForUser(context *gin.Context) {
	var newUser entities.User
	if err := context.ShouldBindJSON(&newUser); err != nil {
		context.AbortWithStatusJSON(http.StatusBadRequest, helpers.BuildResponseError(constants.API_STATUS_CANNOTREAD, constants.MESSAGE_RESPONSE_ERROR_DATA, "", helpers.EmptyObjec{}))
	} else {
		nUser, errSer := s.UserService.SignUpForUser(newUser)
		if errSer != nil {
			if utils.IsDuplicateKeyError(errSer) {
				context.AbortWithStatusJSON(http.StatusConflict, helpers.BuildResponseError(constants.API_STATUS_EXISTDATA, constants.MESSAGE_RESPONSE_USER_EXIST, "", helpers.EmptyObjec{}))
				return
			}
			context.AbortWithStatusJSON(http.StatusInternalServerError, helpers.BuildResponseError(constants.API_STATUS_CANNOTREAD, constants.MESSAGE_RESPONSE_FAILURE, "", helpers.EmptyObjec{}))
		} else {
			context.AbortWithStatusJSON(http.StatusOK, helpers.BuildResponse(constants.API_STATUS_SUCCESS, constants.MESSAGE_RESPONSE_SUCCESSFUL, nUser))
		}
	}
}

func UserControllerIml(service userservices.UserService, jwtService jwts.JWTService) UserController {
	return &userService{
		UserService: service,
		JwtService:  jwtService,
	}
}
