package mediacontrollers

import (
	"golang-chat-backend/src/services/mediaservices"
	"net/http"

	"github.com/gin-gonic/gin"
)

type mediaService struct {
	MediaService mediaservices.MediaService
}

// DisplayAudioMedia implements MediaController
func (*mediaService) DisplayAudioMedia(ctx *gin.Context) {
	panic("unimplemented")
}

// DisplayImageMedia implements MediaController
func (s *mediaService) DisplayImageMedia(ctx *gin.Context) {
	url := ctx.Param("url")
	dataImg, _ := s.MediaService.DisplayImageMedia(url)
	// Chuyển hướng trình duyệt đến URL ảnh
	ctx.Data(http.StatusOK, "image/jpeg", dataImg)

}

// DisplayVideoMedia implements MediaController
func (*mediaService) DisplayVideoMedia(ctx *gin.Context) {
	panic("unimplemented")
}

func MediaControllerIml(mediaSer mediaservices.MediaService) MediaController {
	return &mediaService{
		MediaService: mediaSer,
	}
}
