package mediacontrollers

import "github.com/gin-gonic/gin"

type MediaController interface {
	DisplayImageMedia(ctx *gin.Context)
	DisplayVideoMedia(ctx *gin.Context)
	DisplayAudioMedia(ctx *gin.Context)
}
	