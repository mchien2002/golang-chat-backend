package main

import (
	"golang-chat-backend/src/configs"
	"golang-chat-backend/src/constants"
	"golang-chat-backend/src/controllers/mediacontrollers"
	"golang-chat-backend/src/controllers/usercontrollers"
	"golang-chat-backend/src/middlewares"
	"golang-chat-backend/src/reponsitories/firebaserepositories"
	"golang-chat-backend/src/reponsitories/userrepositories"
	"golang-chat-backend/src/services/jwts"
	"golang-chat-backend/src/services/mediaservices"
	"golang-chat-backend/src/services/userservices"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

// MONGODB DATABASE
var (
	mongoDB        = configs.SetUpMongodbConnection()
	fireBaseBucket = configs.SetupFirebaseBucketStorage()
)

// REPOSITORIES
var (
	firebaseRepository firebaserepositories.FirebaseRepository = firebaserepositories.FirebaseRepositoryIml(fireBaseBucket)
	userRepository     userrepositories.UserRepository         = userrepositories.UserRepositoryIml(mongoDB)
	contactRepository  userrepositories.ContactRepository      = userrepositories.ContactRepositoryIml(mongoDB)
)

// SERVICES
var (
	jwtService     jwts.JWTService             = jwts.JWTServiceIml()
	userService    userservices.UserService    = userservices.UserServiceIml(userRepository, jwtService, firebaseRepository)
	contactService userservices.ContactService = userservices.ContactServiceIml(contactRepository)
	mediaService   mediaservices.MediaService  = mediaservices.MediaServiceIml(fireBaseBucket)
)

// CONTROLLERS
var (
	userController    usercontrollers.UserController    = usercontrollers.UserControllerIml(userService, jwtService)
	contactController usercontrollers.ContactController = usercontrollers.ContactControllerIml(contactService, jwtService)
	mediaController   mediacontrollers.MediaController  = mediacontrollers.MediaControllerIml(mediaService)
)

func main() {
	defer configs.CloseConnection(mongoDB)
	router := gin.Default()
	rest := router.Group("/api/v2")
	rest.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "HI, HAVE A GOOD TRIP <3"})
	})

	router.GET("/ws", func(c *gin.Context) {
		conn, err := configs.Upgrader.Upgrade(c.Writer, c.Request, nil)
		if err != nil {
			return
		}
		// Xử lý logic WebSockets tại đây
		go configs.HandleWebSocket(conn, c)
	})

	restUser := rest.Group(constants.USER_ENDPOINT)
	{
		restUser.POST(constants.USER_SIGNUP, userController.SignUpForUser)
		restUser.POST(constants.USER_SIGNIN, userController.SignInForUser)
		restUser.POST(constants.USER_UPLOAD_AVATAR, middlewares.AuthorJWT(jwtService), userController.UploadUserAvatar)
		restUser.GET("/:userId", middlewares.AuthorJWT(jwtService), userController.SearchUserByIdWithRelationship)
	}

	restContact := rest.Group(constants.CONTACT_ENDPOINT)
	{
		restContact.POST(constants.USER_SEND_CONTACT_INVITATION+"/:friendUin", middlewares.AuthorJWT(jwtService), contactController.UserSendContactInvitation)
		restContact.POST(constants.USER_CONFIRM_CONTACT_INVITATION+"/:friendUin", middlewares.AuthorJWT(jwtService), contactController.UserConfirmContactInvitation)
	}

	restMediaImage := rest.Group(constants.MEDIA_ENDPOINT).Group(constants.IMAGE_ENDPOINT)
	{
		restMediaImage.GET(constants.MEDIA_DISPLAY+"/:url", mediaController.DisplayImageMedia)
	}
	restMediaVideo := rest.Group(constants.MEDIA_ENDPOINT).Group(constants.VIDEO_ENDPOINT)
	{
		restMediaVideo.GET(constants.MEDIA_DISPLAY+"/:url", mediaController.DisplayVideoMedia)
	}
	restMediaAudio := rest.Group(constants.MEDIA_ENDPOINT).Group(constants.AUDIO_ENDPOINT)
	{
		restMediaAudio.GET(constants.MEDIA_DISPLAY+"/:url", mediaController.DisplayAudioMedia)
	}
	router.Run(":" + os.Getenv("PORT"))
}
